#
# varia.sh
#
# All sorts of utilities that can't be grouped together
# in another way.
#

start-test-scripts() {
	cd /c/Users/pieterjan.smets/Documents/projects/multiassist/multiassist-systemtest/src/test/resources/scripts
	start RemoteApplicationStarter-1.0-SNAPSHOT.jar
}


is-machine-simulation-enabled() {
	grep -P "^simulation\s*=\s*true$" "$DVTLS_DB_FOLDER/multiassist.properties"
	if [[ "$?" == "0" ]]
	then    
		echo "true"
	else   
		echo "false"
	fi	
}

# Function that you can use to print '(release new DI)' when
# the folder where all the beta versions are kept has two or
# more major/minor versions.
# Use this by calling the function in your PS1 variable:
# export PS1="$PS1 \$(__new_di_ps1)"
__new_di_ps1() {
	beta_folder='/z/RoboJob/100. Production Software/DEV INT versions/Multi-Assist'
	if [[ -d "$beta_folder" ]]; then
		number_of_versions=$( ls "$beta_folder" | grep -Po "v\d+\.\d+\.\d+" | uniq | wc -l )
		if (( $number_of_versions > 1 )); then
			echo "(release new DI)"
		else
			echo ""
		fi
	else
		echo ""
	fi
}

# Function that you can use to print '(release new initializer)'
# when the folder where all data files are kept has two or more
# major/minor versions.
# Use this by calling the function in your PS1 variable:
# export PS1="$PS1 \$(__new_initializer_ps1)"
__new_initializer_ps1() {
	beta_folder="/z/RoboJob/100. Production Software/ROB CONTR versions/Multi-Assist/initializer versions"
	if [[ -d "$beta_folder" ]]; then
		number_of_versions=$( ls "$beta_folder" | grep -Po ".*\d+\.\d+\.\d+" | uniq | wc -l )
		if (( $number_of_versions > 1 )); then
			echo "(release new initializer)"
		else
			echo ""
		fi
	else
		echo ""
	fi
}
