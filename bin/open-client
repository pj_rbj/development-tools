#!/bin/env python

import os
import sys
import re
from subprocess import Popen
from pathlib import Path


################################################################################
### Constants
################################################################################

BASE_DIR = Path('//alu-srv-file01/data_robocopy/RoboJob/230. Installaties')
REQUESTED_CLIENT = sys.argv[1]


################################################################################
### Functions
################################################################################

def is_country_folder(folder_name):
    pattern = re.compile('^[A-Z]{2} \\(.+\\)')
    return pattern.match(folder_name) is not None

def is_matching_serial(installation):
    pattern = re.compile(REQUESTED_CLIENT, flags=re.IGNORECASE)
    return pattern.search(installation) is not None

# TODO: clean up this mess of a function
def get_matching_serial(country):
    country_clients = [c for c in os.listdir(BASE_DIR / country) if os.path.isdir(BASE_DIR / country /c)]
    return [c + "/" + installation for c in country_clients for installation in os.listdir(BASE_DIR / country / c) if is_matching_serial(c + "/" + installation) and os.path.isdir(BASE_DIR / country / c / installation)]

# Returns a list of files and directories in the given
# directory path. It ignores the '.DS_Store' files 
# that can be found in a lot of directories.
# (no idea what they're there for)
def listdir(path):
    directories = os.listdir(path)
    return [x for x in directories if x != '.DS_Store']

# Some clients only have one installation, with only one
# machine. If that's the case, we can already open that
# installation's folder instead of the top level client
# folder. This function looks for the deepest path we
# can open.
def create_target_dir(client_path):
    current_path = client_path
    subfolders = listdir(current_path)
    while len(subfolders) == 1:
        current_path = current_path / subfolders[0]
        subfolders = listdir(current_path)
    return current_path

# Open a client's directory in Windows explorer
def open_explorer(client):
    client_dir = create_target_dir(BASE_DIR / client)
    print('Opening path: %s' % client_dir)
    explorer_command = 'explorer %s' % client_dir
    Popen(explorer_command)

def die(message=""):
    print("[ERROR] %s" % message)
    exit(1)

def open_candidate(candidates):
    if len(candidates) == 1:
        open_explorer(candidates[0])
    
    else:
        print('Found multiple candidates: ')
        for idx, client in enumerate(candidates):
            print("%s.\t%s" % (idx, client))
    
        requested_index = int(input("Enter the number you want to open: "))
        open_explorer(candidates[requested_index])



################################################################################
### Main script
################################################################################

if __name__ == '__main__':
    try:
        installations = listdir(BASE_DIR)
    except FileNotFoundError:
        die("Can't open installation folder. Are you connected to the Z-drive?")
    countries = [x for x in installations if is_country_folder(x)]
    
    candidates = []
    for c in countries:
        clients = get_matching_serial(c)
        candidates += [Path(c) / client for client in clients]

    if len(candidates) > 0:
        open_candidate(candidates)
        exit(0)

    else:
        print('No client/serial found that matches "%s"' % REQUESTED_CLIENT)

