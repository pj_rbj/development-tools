#
# main.sh
#
# Entry point for this project. It should not implement any
# logic, only source other files and set some variables.
#

export PATH="$PATH:$SCRIPT_DIR_PATH/bin"

source "$SCRIPT_DIR_PATH/user_config.sh"
source "$SCRIPT_DIR_PATH/script_utils.sh"
source "$SCRIPT_DIR_PATH/autocomplete.sh"
source "$SCRIPT_DIR_PATH/navigation.sh"
source "$SCRIPT_DIR_PATH/database.sh"
source "$SCRIPT_DIR_PATH/roboguide.sh"
source "$SCRIPT_DIR_PATH/varia.sh"
source "$SCRIPT_DIR_PATH/java.sh"
