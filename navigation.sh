#
# naviagtion.sh
#
# Some helper functions for navigating the file tree.
# 


############################################################
# p
#
# Navigate to a folder in your projects folder. The 
# projects folder is defined by $PROJECTS_FOLDER.
# Autocomplete is available for this command.
#
# Example usage: 'p my_awesome_project'
############################################################
p() {
	cd "$DVTLS_PROJECTS_FOLDER/$1"
}


############################################################
# pexp
#
# Does the same thing as 'p', but also opens an explorer
# window.
#
# Example usage: 'pexp my_awesome_project'
############################################################
pexp() {
	p "$1"
	explorer .
}


############################################################
# exp
#
# Opens an explorer window in the folder of the given
# project.
#
# Example usage: 'exp my_awesome_project'
############################################################
exp() {
	p "$1"
	explorer .
	cd -
}


# autocomplete for the functions above
__p_autocomplete() {
	local cur prev projects
	projects=`ls $DVTLS_PROJECTS_FOLDER`

	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	COMPREPLY=( $(compgen -W "${projects}" -- ${cur}) )

	return 0
}
complete -o nospace -F __p_autocomplete p
complete -o nospace -F __p_autocomplete pexp
complete -o nospace -F __p_autocomplete exp


